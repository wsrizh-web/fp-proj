module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
        ? './'
        : '/',
    devServer: {
        overlay: {
            warnings: true,
            errors: false
        }
    },
    css: {
        requireModuleExtension: false
    }
}