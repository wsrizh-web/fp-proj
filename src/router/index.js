import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from "@/views/Login";
import Register from "@/views/Register";
import Search from "@/views/Search";
import Profile from "@/views/Profile";
import BookingCreate from "@/views/BookingCreate";
import BookingManagement from "../views/BookingManagement";
import Seats from "../views/Seats";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/search',
    name: 'Search',
    component: Search
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile
  },
  {
    path: '/bookingCreate',
    name: 'BookingCreate',
    component: BookingCreate
  },
  {
    path: '/bookingManagement',
    name: 'BookingManagement',
    component: BookingManagement
  },
  {
    path: '/seats',
    name: 'Seats',
    component: Seats
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

export default router
